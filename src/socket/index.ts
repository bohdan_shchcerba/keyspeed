import {Server} from 'socket.io';
import * as config from './config';
import {stringify} from "querystring";
import {texts} from "../data";
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME} from "./config";

const errorHandler = (socket, type, message) => {
    socket.emit('error', {
        type,
        message
    })
}

interface room {
    name: string,
    numberOfUsers: number,
    users: Array<{ username: string, isReady: boolean, userStatus: number }>,
    countReady: number,
    gameStarted: boolean,
    textId: number
    winners: Array<string>
}

let current_room: string

let users = new Map


let rooms: Array<room> = []

const searchRoomByUsername = (rooms, username) => {
    rooms.map(room => {
        room.users.map(user => {
            if (JSON.parse(user.username) === username) {
                current_room = room.name
            }
        })


    })
}

const deleteUserFromRoom = (socket, io, rooms, roomName, username) => {
    rooms = rooms.map(room => {
        if (room.name === roomName) {
            room!.numberOfUsers = room!.numberOfUsers - 1

            if (room.numberOfUsers > 0) {

                let i = 0
                room?.users.map(user => {
                    const name = JSON.parse(user.username)
                    if (name === username) {
                        room?.users.splice(i, 1)
                    }
                    i++
                })
            }
        }
        return room
    })

    let j = 0
    rooms.map(room => {
        if (room.name === roomName) {
            if (room.numberOfUsers <= 0) {
                rooms.splice(j, 1)
            }
        }
        j++
    })

    let room = rooms.find(room => room.name === roomName)

    io.to(roomName).emit('update_room', room)
    socket.leave(roomName)
    socket.join('lobby')
    io.emit('update_rooms', rooms)

    return rooms
}


export default (io: Server) => {
    io.on('connection', socket => {

        let username = socket.handshake.query.username;

        if (users.get(username)) {
            return errorHandler(socket, 'user', 'username exist')
        }

        users.set(username, 'connected')

        socket.emit('update_rooms', rooms)


        socket.join('lobby')

        socket.on('logout', () => {
            users.delete(username)
        })

        socket.on('create_room', (roomName) => {

            if (rooms.find(room => room.name === roomName)) {
                return errorHandler(socket, 'room', `room with name '${roomName}' exist`)
            }

            const new_room: room = {
                name: roomName,
                numberOfUsers: 0,
                users: [],
                countReady: 0,
                gameStarted: false,
                textId: 0,
                winners: []
            }

            rooms.push(new_room)
            io.emit('update_rooms', rooms)
        })

        socket.on('join', (roomName) => {


            rooms = rooms.map(room => {

                if (room.numberOfUsers >= MAXIMUM_USERS_FOR_ONE_ROOM){
                    errorHandler(socket,'users','room is full')

                }
                else {
                    if (room.name === roomName) {
                        room!.numberOfUsers = room!.numberOfUsers + 1

                        room?.users.push({
                            username: JSON.stringify(username),
                            isReady: false,
                            userStatus: 0
                        })

                    }
                }
                return room
            })

            let room = rooms.find(room => room.name === roomName)

            socket.leave('lobby')
            socket.join(roomName)


            socket.emit('user_connect', room)

            io.emit('update_rooms', rooms)
            io.to(roomName).emit('update_room', room)

        })

        socket.on('leave_room', (roomName) => {

            rooms = deleteUserFromRoom(socket, io, rooms, roomName, username)
        })

        socket.on('ready', (roomName, ready) => {
            rooms = rooms.map(room => {
                if (room.name === roomName) {

                    ready ? room.countReady++ : room.countReady--

                    room?.users.map(user => {
                        const name = JSON.parse(user.username)
                        if (name === username) {
                            user.isReady = ready
                        }
                    })
                    if (room?.numberOfUsers > 1) {

                        if (room?.numberOfUsers === room?.countReady) {
                            room.textId = Math.floor(Math.random() * texts.length)
                            room.gameStarted = true
                        }
                    }

                }
                return room
            })

            let room = rooms.find(room => room.name === roomName)

            if (room!.gameStarted) {
                io.emit('update_rooms', rooms)
            }

            io.to(roomName).emit('update_room', room)

        })

        socket.on('start_game', (roomName) => {
            let room = rooms.find(room => room.name === roomName)

            let count = SECONDS_FOR_GAME
            let myTimer = setInterval(() => {
                if (count <= 0) {
                    clearInterval(myTimer)
                    if (room?.gameStarted) {
                        room!.gameStarted = false

                        let new_rooms = rooms.map(rm => {
                                if (rm.name === roomName) {
                                    return room
                                }
                                return rm
                            }
                        )
                        console.log(new_rooms)
                        // @ts-ignore
                        rooms = new_rooms
                        io.emit('update_rooms', new_rooms)
                        return io.to(roomName).emit('finish_game', room?.winners)

                    }
                } else {
                    if (!room?.gameStarted) {
                        clearInterval(myTimer)
                        return
                    }
                    count--
                    io.to(roomName).emit('update_timer', count)
                }
            }, 1000);

        })

        socket.on('update_status', ({roomName, status}) => {
            let room = rooms.find(room => room.name === roomName)

            let users = room!.users

            users = users.map(user => {
                if (username === JSON.parse(user.username)) {
                    user.userStatus = user.userStatus + status

                    if (user.userStatus >= 100) {
                        room?.winners.push(user.username)

                        if (room?.winners.length === room?.numberOfUsers) {
                            room!.gameStarted = false
                            io.to(roomName).emit('finish_game', room?.winners)
                            // @ts-ignore
                            rooms = rooms.map(rm => {
                                if (rm.name === roomName) {
                                    return room
                                }
                            })
                            io.emit('update_rooms', rooms)
                        }

                    }
                }


                return user
            })

            room!.users = users

            io.to(roomName).emit('update_user_status', room)
        })

        socket.on('reset', roomName => {

            rooms = rooms.map(room => {
                if (room.name === roomName) {
                    room.countReady = 0
                    room.gameStarted = false
                    room.textId = 0
                    room.winners = []

                    room.users = room.users.map(user => {
                        user.isReady = false
                        user.userStatus = 0

                        return user
                    })
                }

                return room
            })

            let room = rooms.find(room => room.name === roomName)

            io.to(roomName).emit('update_room', room)
            io.emit('update_rooms', rooms)


        })

        socket.on("disconnect", () => {
            searchRoomByUsername(rooms, username)

            if (current_room) {
                rooms = deleteUserFromRoom(socket, io, rooms, current_room, username)
            }
            current_room = ''
            users.delete(username)
        })

    });


};
