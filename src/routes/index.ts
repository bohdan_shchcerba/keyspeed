import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';
import { Express } from 'express';
import textsRoutes from "./textsRoutes";

export default (app: Express) => {
	app.use('/login', loginRoutes);
	app.use('/game', gameRoutes);
	app.use('/game/texts', textsRoutes);
};
