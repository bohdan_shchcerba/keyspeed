import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config';
import {texts} from "../data";

const router = Router();

router.get('/:id', (req, res) => {

    const index = Number(req?.params?.id)
    const l =texts?.length
    if (index <= l && index >= 0){
        res.send(JSON.stringify(texts[index]));
    }

});

export default router;
