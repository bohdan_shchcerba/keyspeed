import {showInputModal, showMessageModal} from "./modal.mjs";
import {appendRoomElement} from "./room.mjs";



export const lobbySocketHandler = (socket) => {

    const create_btn = document.getElementById("add-room-btn")
    const rooms_container = document.getElementById("rooms-wrapper")

    const createRoom = () => {
        showInputModal({
            title: "create", onChange: (e) => {
                socket.emit('create_room', e)
                onJoin(e)
            }
        })

    }


    const onJoin = (roomName) => {
        socket.emit('join', roomName)
        sessionStorage.setItem('room_name', roomName)
    }


    const showRooms = (rooms) => {
        rooms_container.innerHTML = ''
        rooms?.map(room => {
            if (room.gameStarted || room.numberOfUsers >= 10){
                return
            }
            appendRoomElement({name: room.name, numberOfUsers: room.numberOfUsers, onJoin})
        })

    }

    socket.on('update_rooms', showRooms)

    create_btn.addEventListener('click', createRoom)

}
