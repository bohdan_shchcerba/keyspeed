import {showMessageModal, showResultsModal} from "./views/modal.mjs";
import {lobbySocketHandler} from "./views/lobby.mjs";
import {appendUserElement, setProgress} from "./views/user.mjs";
import getText from "./helpers/getTextApi.mjs";

const username = sessionStorage.getItem('username');
let ready = false

const room_page = document.getElementById('rooms-page')
const game_page = document.getElementById('game-page')
const btn_quit_room = document.getElementById('quit-room-btn')
const room_name = document.getElementById('room-name')
const room_users = document.getElementById('users-wrapper')
const btn_ready = document.getElementById('ready-btn')
const timer = document.getElementById('timer')
const text_container = document.getElementById('text-container')
const game_timer_seconds = document.getElementById('game-timer-seconds')
const game_timer = document.getElementById('game-timer')
if (!username) {
    window.location.replace('/login');
}
const socket = io('http://localhost:3002', {query: {username: username}});


export const socketErrorHandler = (socket) => {

    socket.on('error', (err) => {

        if (err.type === 'user') {
            sessionStorage.removeItem('username')
            socket.emit('logout')

            showMessageModal({
                message: err.message, onClose: () => {
                    window.location.replace('/login');
                }
            })
        } else if (err.type === 'room' || err.type === 'users') {
            showMessageModal({
                message: err.message, onClose: () => {
                    window.location.replace('/game');

                }
            })
        }

    })
}


const quit_room = () => {
    game_page.classList.add('display-none')
    room_page.classList.remove('display-none')

    const roomName = sessionStorage.getItem('room_name')

    socket.emit('leave_room', roomName)
}


const ready_handler = () => {
    ready = !ready
    btn_ready.innerText = ready ? 'READY' : 'NOT READY'
    const roomName = sessionStorage.getItem('room_name')
    socket.emit('ready', roomName, ready)

}


const startGame = async () => {
    btn_quit_room.classList.add('display-none')
    btn_ready.classList.add('display-none')
    timer.classList.remove('display-none')

    let count = 10

    const roomName = sessionStorage.getItem('room_name')


    text_container.innerText = ''
    const text = await getText()
    const characters = text.split("").map((char) => {
        const span = document.createElement("span");
        span.innerText = char;
        text_container.appendChild(span);
        return span;
    });

    let cursorIndex = 0;
    let cursorCharacter = characters[cursorIndex];
    cursorCharacter.classList.add("cursor");

    const keydown = ({key}) => {

        if (key === cursorCharacter.innerText) {
            cursorCharacter.classList.remove("cursor");
            cursorCharacter.classList.remove('incorrect')
            cursorCharacter.classList.add('correct')
            cursorCharacter = characters[++cursorIndex];

            let status = 100 / characters.length

            socket.emit('update_status', {roomName, status: status})

        } else {
            cursorCharacter.classList.add('incorrect')

        }

        if (cursorIndex >= characters.length) {
            // game ended
            console.log('finish')

        }

        cursorCharacter?.classList.add("cursor");
    };

    let myTimer = setInterval(() => {
        if (count >= 0) {
            timer.innerText = JSON.stringify(count--)
        } else {
            clearInterval(myTimer)

            timer.classList.add('display-none')
            game_timer.classList.remove('display-none')

            text_container.classList.remove('display-none')
            socket.emit('start_game', roomName)
            document.addEventListener('keydown', keydown)
        }
    }, 1000);


}

const update_room = async (room) => {
    room_users.innerHTML = ''
    timer.innerText = '10'

    if (room?.gameStarted) {
        await startGame()
    }

    room?.users?.map(user => {
        const currentUser = JSON.parse(user.username) === username

        appendUserElement({username: JSON.parse(user.username), ready: user?.isReady, isCurrentUser: currentUser})


    })
}

const user_connect = (room_data) => {
    room_page.classList.add('display-none')
    game_page.classList.remove('display-none')
    room_name.innerText = room_data.name
    room_data.users.map(user => {
        const currentUser = JSON.parse(user.username) === username

        appendUserElement({username: JSON.parse(user.username), ready: false, isCurrentUser: currentUser})
    })
}

lobbySocketHandler(socket)
socketErrorHandler(socket)

btn_quit_room.addEventListener('click', quit_room)
btn_ready.addEventListener('click', ready_handler)

socket.on('user_connect', user_connect)
socket.on('update_room', update_room)
socket.on('update_user_status', (room) => {

    room?.users.map(user => {
        setProgress({username: JSON.parse(user.username), progress: user.userStatus})

    })
})

const afterFinishGame = async () => {


    game_timer.classList.add('display-none')
    text_container.classList.add('display-none')
    btn_ready.classList.remove('display-none')
    btn_quit_room.classList.remove('display-none')
    window.addEventListener('click', function(event) {
        event.stopImmediatePropagation();
    }, true);
    const roomName = sessionStorage.getItem('room_name')
    ready = false
    socket.emit('reset',roomName)

}

socket.on('finish_game', winners => {
    showResultsModal({usersSortedArray: winners, onClose: afterFinishGame})

})

socket.on('update_timer',(time)=>{

    game_timer_seconds.innerText = time
})
